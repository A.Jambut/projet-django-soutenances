# -*- coding: utf-8 -*-
# Generated by Django 1.11.5 on 2017-10-18 08:14
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Enseignant',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nom', models.CharField(max_length=64)),
                ('prenom', models.CharField(max_length=64)),
                ('photo', models.ImageField(upload_to='')),
                ('detail', models.CharField(max_length=200)),
            ],
        ),
        migrations.CreateModel(
            name='Etudiant',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nom', models.CharField(max_length=64)),
                ('prenom', models.CharField(max_length=64)),
                ('photo', models.ImageField(upload_to='')),
                ('detail', models.CharField(max_length=200)),
            ],
        ),
        migrations.CreateModel(
            name='Evenement',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nom', models.CharField(max_length=64)),
                ('date_debut', models.DateField()),
                ('date_fin', models.DateField()),
                ('enseignant', models.ForeignKey(to='Soutenances.Enseignant')),
            ],
        ),
        migrations.CreateModel(
            name='Groupe',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nom', models.CharField(max_length=64)),
            ],
        ),
        migrations.CreateModel(
            name='Slots',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date', models.DateField()),
                ('heure_debut', models.TimeField()),
                ('heure_fin', models.TimeField()),
                ('evenement', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='Soutenances.Evenement')),
            ],
        ),
        migrations.CreateModel(
            name='Soutenance',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('duree', models.DurationField()),
                ('salle', models.CharField(max_length=64)),
                ('etudiants', models.ManyToManyField(to='Soutenances.Etudiant')),
            ],
        ),
        migrations.AddField(
            model_name='etudiant',
            name='groupe',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='Soutenances.Groupe'),
        ),
    ]
