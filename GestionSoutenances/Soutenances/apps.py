from django.apps import AppConfig


class SoutenancesConfig(AppConfig):
    name = 'Soutenances'
